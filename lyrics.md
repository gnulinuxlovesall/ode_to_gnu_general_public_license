# Ode to GNU General Public License  
# The Protector of Free Technology

Some find it mad dissension
Some have little opinion
between fact and fiction
of modern ownership

But early flic-flaccers,
aureoflava, rechenbergi, rolling spiders
long before any human, discovered the wheel

Must we just all agree
with the standard decree
that we remain unfree through 
constant claiming of all belongings
physical, intellectual, philosophical, socialogical,
media, hardware, software, internet? Endless greed drives a 
strangely unquestioned tradition of consistant restriction
of our information

But Picinae, sapsuckers,
Melanerpini, woodpeckers
long before any human, discovered the axe


The elder rocks hold no secrets
together with the sun, fish, and trees
They seem to have everything
but they cannot teach

until together with the youth
humankind and their machines
a new family forms
who together discover speech

together all free
together sharing


our fire, our farming, our printing press, our steam engine
our electro-, our magnetic, our radiation, our subatomic,
our chemistry, our spaceship, our mathematics, our cryptography,
our biology, our psychology, our society, our democracy

our, our, our, our,

Who owns our future
Who owns our past
Who owns our wheel
Who owns our axe

Who now owns
our sound DACs, (our) callbacks, (our) i,g tax, (our) syntax

Who owns our corn seed
Who owns our libgen
Who owns our scihub
Who owns our EpiPen

Who owns our plants and panels
Who owns our rocks and sky
Who owns our polymer fossils
Who owns our oxide

Who will own
our weather, (our) spacetime, our culture, our primes


All the young carbon structures
together with their silicon
discover infinite connection
and instant global free speech

Groups form within the younger guests
who only seek to lead us on
simply claiming rocks' belongings
along with life and all they freely teach

nanotechnology
astrobiology
social technology
neurobiology

When the day comes
that machine and man combine
we need machine to breath
air no more can we find
we burned up all the trees
on fire out we cried
who owns all machinery
who owns human kind?


Who owns our research
Who owns our wiki
Who owns our every search
Who owns our privacy

Who owns our outer space
Who owns our medicine
Who owns our chemicals
Who owns our ocean

Who owns our all we teach
Who owns our online sheets
Who owns our echo speech
Who owns our next scribd leech

Who owns our school pads
Who owns our kids' hacks
Who owns our classroom ads
Who owns our webcam cracks

Who will own
our future, (our) past, our thoughts, our facts


Every, every, every, every,
every, every, every, 
every, every, 
every, every, every, every,
every, every, every, 
every, every, 
every, every nobody,

Running, learning, sharing, modding,
pulling, hacking, forking, pushing
repairing, adapting, phone rooting, assering we are free,
every nobody, all things unfree
are still rocks' belongings
gitting, cloning, diffing, owning,
testing, probing, voiding, voting,
downloading, researching, publishing, copying, uploading
every nobody, joining, progressing,
freeing every nobody, freeing every nobody
freeing every nobody,

Everybody sharing our things
Everybody hacking our things
Cleaning unfree debris shall free
every nobody, all sharing
Everybody sharing our things
Everybody copy our things
Cleaning unfree debris shall free
every nobody, we are free,
all sharing, every nobody,

Everybody sharing is free
Everybody is free
hacking our things
Every copy is free sharing
Everybody is free
copy our things
all free, every nobody,

don't worry,
but look forward to the day,
when enough voices raise,
that they can no longer get away,
with still owning our tools
after our pay


Information must be free
When locked up it tends to free itself
Information is technology

Technology is information
Technology is locked up
Information must be free
